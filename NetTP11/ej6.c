#include <stdio.h>
#include <stdlib.h>
#include <allegro5/allegro.h>
#include <allegro5/allegro_color.h>
#include <allegro5/allegro_primitives.h>
#include "ej5.h"                                                    //libreria usada para las funciones principales del programa

#define FPS 2.0
#define D_WIDTH 1001                                                //constante ancho display
#define D_HEIGHT 200                                                //constante alto display
#define PORT 'A'                                                    //constante port A

int leer(int *quitflag);                                            //funcion que interpreta el valor ingresado
void apagado(void);                                                 //funcion que apaga los LEDs 
void imprime(void);                                                 //funcion que imprime los LEDs conectados al puerto A

int main(void)
{
    int mask = 0xFF;                                                //mascara para modificar todos los bits
    int flagpar = 0;                                                //flag para el parpadeo de los LEDs usando el timer
    
    ALLEGRO_DISPLAY *display = NULL;
    ALLEGRO_EVENT_QUEUE *event_queue = NULL;
    ALLEGRO_TIMER *timer = NULL;
    
    bool tecla_b = false;                                           //guarda el estado de la tecla b
    bool redraw = false;
    bool do_exit = false;
    
    if(!al_init())                                                  //inicializo allegro y todas las funciones usadsas
    {
        fprintf(stderr, "failed to initialize allegro!\n");
        return -1;
    }
    
    if(!al_install_keyboard())
    {
        fprintf(stderr, "failed to initialize the keyboard!\n");
        return -1;
    }
    
    timer = al_create_timer(1.0 / FPS);                             //creamos el timer
    
    if(!timer)
    {
        fprintf(stderr, "failed to create timer!\n");
        return -1;
    }
    
    event_queue = al_create_event_queue();                          //cramos cola de eventos
    
    if(!event_queue)
    {
        fprintf(stderr, "failed to create event_queue!\n");
        al_destroy_timer(timer);
        return -1;
    }
    
    if(!al_init_primitives_addon())
    {
        fprintf(stderr, "failed to initialize primitives!\n");
        al_destroy_timer(timer);
        al_destroy_event_queue(event_queue);
        return -1;
    }

    display = al_create_display(D_WIDTH, D_HEIGHT);                 //creamos el display

    if(!display)
    {
        fprintf(stderr, "failed to create display!\n");
        al_destroy_timer(timer);
        al_destroy_event_queue(event_queue);
        al_shutdown_primitives_addon();
        return -1;
    }
    
    al_register_event_source(event_queue, al_get_display_event_source(display));
    al_register_event_source(event_queue, al_get_timer_event_source(timer));
    al_register_event_source(event_queue, al_get_keyboard_event_source());
    
    al_clear_to_color(al_map_rgb(64, 64, 64));                      //limpiamos el buffer al color indicado
    
    for(int j = 0; j < 8; j++)                                      //mostramos los LEDs en el buffer
    {
        al_draw_filled_circle((D_WIDTH-63)-125*j, (D_HEIGHT / 2)+3, 50.0, al_color_name("grey"));       //sombra del LED
        al_draw_filled_circle((D_WIDTH-63)-125*j, D_HEIGHT / 2, 50.0, al_color_name("black"));          //LED
    }
    
    al_flip_display();                                              //pasamos el contenido del buffer al display para tener una vista previa del formato de los LEDs
    al_start_timer(timer);                                          //comienza el contador
    
    while (!do_exit) 
    {
        ALLEGRO_EVENT ev;
        if(al_get_next_event(event_queue, &ev))                     //Toma un evento de la cola
        {
            if(ev.type == ALLEGRO_EVENT_TIMER)
            {
                redraw = true;
            }
            else if(ev.type == ALLEGRO_EVENT_DISPLAY_CLOSE)         //evaluamos si se cerro el display con el boton x de arriba a la derecha
            {
                do_exit = true;                                     //de ser asi finaliza el programa
            }
            else if(ev.type == ALLEGRO_EVENT_KEY_DOWN)              //evaluamos que tecla fue presionada
            {
                switch(ev.keyboard.keycode)
                {
                    case ALLEGRO_KEY_S:
                        maskon(PORT, mask);                         //funcion que enciende todos los LEDs
                        imprime();                                  //imprimimos el estado actual del puerto A
                        break;

                    case ALLEGRO_KEY_C:
                        maskoff(PORT, mask);                        //funcion que apaga todos los LEDs
                        imprime();
                        break;

                    case ALLEGRO_KEY_T:
                        masktoggle(PORT, mask);                     //funcion que cambia el estado de los LEDs
                        imprime();
                        break;

                    case ALLEGRO_KEY_0:
                        bitset(PORT, 0);                            //funcion que enciende el LED correspondiente al bit indicado 
                        imprime();
                        break;
                        
                    case ALLEGRO_KEY_1:
                        bitset(PORT, 1);
                        imprime();
                        break;
                        
                    case ALLEGRO_KEY_2:
                        bitset(PORT, 2);
                        imprime();                        
                        break;
                        
                    case ALLEGRO_KEY_3:
                        bitset(PORT, 3);
                        imprime();
                        break;
                        
                    case ALLEGRO_KEY_4:
                        bitset(PORT, 4);
                        imprime();
                        break;
                        
                    case ALLEGRO_KEY_5:
                        bitset(PORT, 5);
                        imprime();
                        break;
                        
                    case ALLEGRO_KEY_6:
                        bitset(PORT, 6);
                        imprime();
                        break;
                        
                    case ALLEGRO_KEY_7:
                        bitset(PORT, 7);
                        imprime();
                        break;
                    
                    case ALLEGRO_KEY_B:
                        tecla_b = (!tecla_b);                       //al precionar la tecla B tecla_b cambia de estado inidcando si deben parapdear o no los LEDs
                        break;
                }
            }
            else if (ev.type == ALLEGRO_EVENT_KEY_UP)               //evaluamos que tecla se dejo de presionar
            {
                switch (ev.keyboard.keycode)
                {
                    case ALLEGRO_KEY_Q:
                        do_exit = true;                             //la letra q finaliza el programa
                        break;
                }
            }
        }
        if (redraw && al_is_event_queue_empty(event_queue))         //caundo la cola esta vacia
        {
            redraw = false;
            if((flagpar == 0) && tecla_b)
            {
                flagpar = 1;
                apagado();                                          //apagamos los LEDs
            }
            else if((flagpar == 1) || tecla_b)
            {
                flagpar = 0;
                imprime();                                          //prendemos los LEDs
            }
        }
    }
    
    al_destroy_timer(timer);
    al_destroy_display(display);
    al_shutdown_primitives_addon();
    return 0;
}

void apagado(void)                                                  //funcion que apaga los LEDs
{
    int i;
    
    al_clear_to_color(al_map_rgb(64, 64, 64));                      //limpiamos el buffer al color inidicado
    
    for(i = 7; i >= 0; i--)
    {
        al_draw_filled_circle((D_WIDTH-63)-125*i, (D_HEIGHT / 2)+3, 50.0, al_color_name("grey"));
        al_draw_filled_circle((D_WIDTH-63)-125*i, D_HEIGHT / 2, 50.0, al_color_name("black"));          //apagamos el LED
    }
    
    al_flip_display();                                              //pasamos lo que hay en el buffer al display
}

void imprime(void)                                                  //funcion que imprime los LEDs conectados al puerto A
{
    int i;
    int bit;
    
    printf("LEDs Port A: ");
    
    al_clear_to_color(al_map_rgb(64, 64, 64));                      //limpiamos el buffer al color inidicado
    
    for(i = 7; i >= 0; i--)
    {
        bit = bitget(PORT, i);                                      //le asignamos a bit el valor del bit que deseamos imprimir
        
        printf("%d", bit );                                         //imprimimos el estado de todos los bits del puerto A
        
        if(bit == 1)
        {
            al_draw_filled_circle((D_WIDTH-63)-125*i, (D_HEIGHT / 2)+3, 50.0, al_color_name("grey"));
            al_draw_filled_circle((D_WIDTH-63)-125*i, D_HEIGHT / 2, 50.0, al_color_name("white"));      //prendemos el LED
    
        }
        else
        {
            al_draw_filled_circle((D_WIDTH-63)-125*i, (D_HEIGHT / 2)+3, 50.0, al_color_name("grey"));
            al_draw_filled_circle((D_WIDTH-63)-125*i, D_HEIGHT / 2, 50.0, al_color_name("black"));      //apagamos el LED
    
        }
    }
    
    al_flip_display();                                              //pasamos lo que hay en el buffer al display
    
    printf("\n");
}

